export const SITE_TITLE = "Tardis Project";
export const SITE_DESCRIPTION = `We provide computing services to hobbyists and small organisations, for any non-profit purposes.
Our goal is to promote small-scale computing, and provide a safe space for users to learn practical sysadmin and computing skills.
`;

export const ADMIN_EMAIL = "hello@tardisproject.uk";