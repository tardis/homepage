---
title: Managed MySQL databases
pubDate: 2023-06-04
---

You no longer need to setup your own MySQL server if you need a database - you can create one from the [tardis console](https://console.tardisproject.uk) and access it anywhere inside our network.

The server itself is backed by Percona's Kubernetes operator, which means we can do zero-downtime upgrades and scale it if we need to. It's also monitored and backed up regularly, so you don't have to worry about data going missing.
